package bd.generator.impl;

import bd.generator.DataGenerator;

import java.util.ArrayList;
import java.util.List;

public class CustomerDataGenerator {

    public static void main(String[] args) throws Exception {
        new FirstFormatGenerator(1).generateFiles();
        new SecondFormatGenerator(1).generateFiles();
        new ThirdFormatGenerator(1).generateFiles();
    }

    static class FirstFormatGenerator extends DataGenerator {

        public FirstFormatGenerator(int numFiles) {
            super(numFiles, "first");
        }

        public List<String> generateLine() {
            List<String> line = new ArrayList<String>();
            line.add(getRandomDateDMY(1980));
            line.add(getRandomInt(20));
            line.add(getRandomDouble(100));
            return line;
        }

        public int getNumLines() {
            return 100;
        }
    }

    static class SecondFormatGenerator extends DataGenerator {

        public SecondFormatGenerator(int numFiles) {
            super(numFiles, "second");

        }

        public List<String> generateLine() {
            List<String> line = new ArrayList<String>();
            line.add(getRandomInt(20));
            line.add(getRandomDate(1980));
            line.add(getRandomDouble(100));
            return line;
        }

        public int getNumLines() {
            return 100;
        }
    }


    static class ThirdFormatGenerator extends DataGenerator {

        public ThirdFormatGenerator(int numFiles) {
            super(numFiles, "third");

        }

        public List<String> generateLine() {
            List<String> line = new ArrayList<String>();
            line.add(getRandomInt(100));
            line.add(getRandomDate(20));
            line.add(getRandomDouble(1980));
            return line;
        }

        @Override
        public String getFieldSeparator() {
            return ";";
        }

        public int getNumLines() {
            return 100;
        }
    }
}
