package bd.generator.impl;

import bd.generator.DataGenerator;

import java.util.ArrayList;
import java.util.List;

public class ExpenseCategoryDataGenerator extends DataGenerator {

    private static String[] expenses = {"taxes", "petrol", "internet", "electricity", "groceries", "rent"};


    public ExpenseCategoryDataGenerator() {
        super(1,"expenses");
    }


    public static void main(String[] args) throws Exception {
        new ExpenseCategoryDataGenerator().generateFiles();
    }

    public List<String> generateLine() {
        List<String> line = new ArrayList<String>();
        line.add(getRandomDate(2017));
        line.add(getRandomString(expenses));
        line.add(getRandomDouble(1000));
        return line;
    }

    public int getNumLines() {
        return 500;
    }
}
