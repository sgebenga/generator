package bd.generator.impl;

import bd.generator.DataGenerator;

import java.util.ArrayList;
import java.util.List;

public class JoinDataGenerator {

    private final static String[] names = {"Jacob", "Helen", "Cyril", "Malusi", "Mmusi", "Jackson", "Atul", "Pravin", "Nelson", "Thabo"};
    private final static String[] surnames = {"Mandela", "Mbeki", "Gigaba", "Maimane", "Gupta", "Gordhan", "Zuma", "Mthembu", "Ramaphosa", "Zuma"};

    public static void main(String[] args) throws Exception {
        new CustomersGenerator().generateFiles();
        new PurchasesGenerator().generateFiles();

    }

    static class CustomersGenerator extends DataGenerator {

        private int id;

        public CustomersGenerator() {
            super(1, "customers");
            this.id = 0;
        }

        public List<String> generateLine() {
            List<String> line = new ArrayList<String>();
            line.add(Integer.valueOf(++id).toString());
            line.add(String.format("%s %s %s", super.getRandomString(names), super.getRandomString(names), super.getRandomString(surnames)));
            line.add(getRandomDate(1980));
            return line;
        }

        public int getNumLines() {
            return 100;
        }
    }

    static class PurchasesGenerator extends DataGenerator {

        public PurchasesGenerator() {
            super(3, "purchases");
        }

        public List<String> generateLine() {
            List<String> line = new ArrayList<String>();
            line.add(getRandomInt(1000));
            line.add(getRandomDate(2017));
            line.add(getRandomDouble(1000));
            return line;
        }

        public int getNumLines() {
            return 500;
        }
    }
}
