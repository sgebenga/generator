package bd.generator.impl;

import bd.generator.DataGenerator;

import java.util.ArrayList;
import java.util.List;

public class SecondarySortDataGenerator extends DataGenerator {

    public static void main(String[] args) throws Exception {
        new SecondarySortDataGenerator(1).generateFiles();
    }

    public SecondarySortDataGenerator(int numFiles) {
        super(numFiles,"secondary_sort");
    }

    public List<String> generateLine() {
        List<String> line = new ArrayList<String>();
        line.add(getRandomInt(100));
        line.add(getRandomDate(2016));
        line.add(getRandomInt(30));
        line.add(getRandomDouble(1000));
        line.add(getRandomDouble(10000));
        return line;
    }

    public int getNumLines() {
        return 1000;
    }
}
